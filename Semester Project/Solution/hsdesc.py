# HEARTHSTONE BATTLEFIELD DESCRIPTOR
import pandas as pd
from hearthstone.sides import *
from hearthstone.processing import *
import hearthstone.tools
import skimage.io
import os.path
import argparse


def is_valid_file(parser, arg):
    if not os.path.exists(arg):
        parser.error("The file %s does not exist!" % arg)
    else:
        return arg


def is_valid_dir(parser, arg):
    if not os.path.isdir(arg):
        parser.error("'%s' is not a valid directory!" % arg)
    else:
        return arg

TRAIN = False

parser = argparse.ArgumentParser(description='Hearthstone Game Descriptor')
parser.add_argument("-f", "--file", dest="filename", required=False,
                    help="run descriptor on given file", metavar="FILE",
                    type=lambda x: is_valid_file(parser, x))
parser.add_argument("-d", "--dir", dest="folder", required=False,
                    help="run descriptor on files in given folder", metavar="DIR",
                    type=lambda x: is_valid_dir(parser, x))
parser.add_argument("-t", "--train", action="store_true", required=False, help="train models before running descriptor")
parser.add_argument("-v", "--visualize", action="store_true", required=False, help="visualize results of classification")
parser.add_argument("-o", "--output", dest="outfile", required=False,
                    help="write results to an output text file", metavar="FILE")
args = parser.parse_args()

necessary_files = ['data/cards_masks', 'data/classes', 'data/attack_bar.png', 'data/health_bar.png',
                   'data/player_mask.png']
for f in necessary_files:
    if not os.path.exists(f):
        print("File or folder '%s' is necessary to run the script..." % f)
        exit(3)

if args.filename is None and args.folder is None:
    parser.error('At least one of --filename or --folder is required to run the script')

if args.filename and args.folder:
    parser.error('You can use either --file or --folder, but not both at the same time')
    exit(1)

if not os.path.exists('models') and not args.train:
    parser.error("Models or folder 'models' not found. Use -t to train required models for classification")
    exit(2)

if args.train:
    TRAIN = args.train
else:
    for i in ['models/attack.pkl', 'models/health.pkl']:
        if not os.path.exists(i):
            print("Models '%s' not found! Use -t to train or retrain models to solve this issue" % i)
            exit(4)

if args.filename and not args.folder:
    try:
        skimage.io.imread(args.filename)
    except IOError:
        parser.error("File '%s' is not a valid image file" % args.filename)

# if args.folder and not args.filename:
#     print('Processing folder')
#     f = args.folder

if args.filename:
    list_files = [args.filename]
else:
    list_files = []
    for f in listdir(args.folder):
        try:
            skimage.io.imread(os.path.join(args.folder, f))
            list_files.append(os.path.join(args.folder, f))
        except IOError:
            print(">>> Omitting '%s' because it is not a valid image file" % f)

if len(list_files) > 0:
    print('>>> [%s] files will be processed\n' % len(list_files))
else:
    print('>>> Nothing to process - terminating process')

# TRAIN = False
health_model = KNNClassifier(n_neighbours=7)
attack_model = KNNClassifier(n_neighbours=5)
if TRAIN:
    if not os.path.exists('models'):
        os.mkdir('models')

    print('>>> Training KNN model for health attributes ...')
    health_indicators = skimage.color.rgb2gray(skimage.io.imread('data/health_bar.png'))
    train_data, train_labels = Factory.split_train(health_indicators, 43, 63, 4, 30)
    train_data, train_labels = Factory.sprout_train(train_data, train_labels)
    health_model.train(train_data, train_labels)
    health_model.save('models/health.pkl')
    print('>>> Trained KNN model for health attributes has been saved!')

    print('>>> Training KNN model for attack attributes ...')
    attack_indicators = skimage.color.rgb2gray(skimage.io.imread('data/attack_bar.png'))
    train_data, train_labels = Factory.split_train(attack_indicators, 43, 46, 4, 30)
    train_data, train_labels = Factory.sprout_train(train_data, train_labels)
    attack_model.train(train_data, train_labels)
    attack_model.save('models/attack.pkl')
    print('>>> Trained KNN model for attack attributes has been saved!')
else:
    print('>>> Loading pretrained models ...')
    health_model.load('models/health.pkl')
    attack_model.load('models/attack.pkl')
    print('>>> Pretrained KNN models have been loaded!')

out_csv = pd.DataFrame()
for i, f in enumerate(list_files):
    print('\n>>> Processing file %s/%s' % ((i+1), len(list_files)))
    img = skimage.io.imread(f)

    # Resize image if it's not Full HD
    img = skimage.transform.resize(img, (1080, 1920), preserve_range=True)
    img = np.asarray(img, dtype=np.uint8)

    print(">>> Recognizing and describing player's objects...")
    p = Player(img, health_model=health_model, attack_model=attack_model)
    print(">>> Recognizing and describing opponent's objects...")
    o = Opponent(img, health_model=health_model, attack_model=attack_model)

    final_desc = dict()
    final_desc.update(p.get_descriptor())
    final_desc.update(o.get_descriptor())
    final_desc['filename'] = f

    if args.visualize:
        print('>>> Visualizing results...')
        hearthstone.tools.visualize_descriptor(final_desc)

    if args.outfile is None:
        hearthstone.tools.print_descriptor(final_desc)
    else:
        final_desc['player minions attribs'] = hearthstone.tools.make_attrs_string(final_desc['player minions attribs'])
        final_desc['opponent minions attribs'] = hearthstone.tools.make_attrs_string(final_desc['opponent minions attribs'])
        out_csv = out_csv.append(pd.DataFrame([final_desc]), ignore_index=True)

if args.outfile is not None:
    out_csv.to_csv(args.outfile, index=False)
    print("\n>>> Results have been written to file: '%s'" % args.outfile)

exit(0)
annonated_data = pd.read_csv('annotated-screenshots.csv')
annonated_data.fillna(value=None, inplace=True)
out = pd.DataFrame()
annonated_data_output = pd.DataFrame()
for row in range(len(annonated_data)):
    if row <= 89:
        continue
    desc = hearthstone.tools.map_values(annonated_data.iloc[row])

    img = skimage.io.imread(desc['filename'])
    print('Processing file: %s' % desc['filename'])

    # Resize image if it's not FullHD
    img = skimage.transform.resize(img, (1080, 1920), preserve_range=True)
    img = np.asarray(img, dtype=np.uint8)

    p = Player(img, health_model=health_model, attack_model=attack_model)
    o = Opponent(img, health_model=health_model, attack_model=attack_model)

    final_desc = dict()
    final_desc.update(p.get_descriptor())
    final_desc.update(o.get_descriptor())
    final_desc['filename'] = desc['filename']
    final_desc['cards set'] = desc['cards set']
    # print(final_desc)
    # print(desc)

    comp = hearthstone.tools.compare_descriptors(desc, final_desc)
    # hearthstone.tools.visualize_descriptor(final_desc)

    out = out.append(pd.DataFrame([comp]), ignore_index=True)

    # print(final_desc['player minions attribs'])
    # print(hearthstone.tools.make_attrs_string(final_desc['player minions attribs']))
    final_desc['player minions attribs'] = hearthstone.tools.make_attrs_string(final_desc['player minions attribs'])
    # print(final_desc['player minions attribs'])
    final_desc['opponent minions attribs'] = hearthstone.tools.make_attrs_string(final_desc['opponent minions attribs'])
    final_desc['filename'] = os.path.basename(final_desc['filename'])
    annonated_data_output = annonated_data_output.append(pd.DataFrame([final_desc]), ignore_index=True)

# TODO: sort columns of out.csv the same as annotated_data.csv
out.to_csv('results2.csv', index=False)
annonated_data_output = annonated_data_output[annonated_data.columns]
annonated_data_output.to_csv('annotated-output2.csv', index=False)
