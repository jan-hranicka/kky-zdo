import numpy as np
from skimage.feature import hog

# Used for saving and loading KNNClassifier models directly to/from HDD
import cPickle

import skimage
import skimage.io
import skimage.color
import skimage.filters
import skimage.transform
import skimage.morphology
import skimage.measure
import skimage.exposure
from skimage.feature import match_template
from sklearn.neighbors import KNeighborsClassifier
from scipy import ndimage
from skimage.filters import sobel
from os import listdir, path

import matplotlib.pyplot as plt


class PartialImage:
    """ Utilization for storing information for cutting off a part of an image """
    _loc = None
    _shape = None

    def __init__(self, loc, shape):
        """
        Initializes 
        :param loc: 
        :param shape: 
        """
        self._loc = loc
        self._shape = shape

    @property
    def loc(self):
        """
        (getter) Get the location property value
        :return: 
        """
        return self._loc

    @property
    def shape(self):
        """ (getter) Get the shape property value """
        return self._shape


class Factory:
    # TODO: Add docstring
    def __init__(self):
        pass

    @staticmethod
    def cut_off_image(image, loc, shape):
        """
        Cut off a part of an image based on location and requested shape
        :param image: Image as an ndarray
        :param loc: Location as a tuple (x, y)
        :param shape: Shape as a tuple (width, height)
        :return: Image cut-off 
        """
        # check whether loc and shape is tuple(2)
        if (type(loc) is tuple and len(loc) == 2) and (type(shape) is tuple and len(shape) == 2):
            if len(image.shape) == 2:
                return image[loc[1]:loc[1]+shape[1], loc[0]:loc[0]+shape[0]]
            else:
                return image[loc[1]:loc[1]+shape[1], loc[0]:loc[0]+shape[0], :]
        else:
            raise ValueError("'loc' and 'shape' arguments must be a tuple (x, y)")

    @staticmethod
    def split_train(image, width, height, nX, nY):
        """
        Split single image pattern into small images representing train data set
        :param image: Image containing grid of train images
        :param width: Width of each of images
        :param height: Height of each of images
        :param nX: Number of images in x-axis
        :param nY: Number of images in y-axis
        :return: Labeled train data set as a tuple (train data, train data labels)
        """
        train_data = []
        train_data_labels = []
        for k in range(0, nY+1):
            for l in range(0, nX):
                img_gray = image[height * k:height * (k + 1), width * l:width * (l + 1)]
                train_data.append(img_gray)
                train_data_labels.append(nY - k)
        return train_data, train_data_labels

    @staticmethod
    def sprout_train(train_data, train_labels):
        """
        Sprout data to get bigger train data set for more robust training of a classifier
        :param train_data: Train data set
        :param train_labels: Train data set labels
        :return: Spouted train data as a tuple (train data, train data labels)
        """
        sprouted_train = []
        sprouted_labels = []
        s = np.int32(train_data[0].shape)
        for i in range(len(train_data)):
            sprouted_train.append(train_data[i])
            sprouted_labels.append(train_labels[i])

            # Add Gaussian blur to sprout train data and make classifier more robust
            from skimage.filters import gaussian
            for sigma in np.linspace(0.5, 1.0, 5):
                sprouted_train.append(gaussian(train_data[i], sigma))
                sprouted_labels.append(train_labels[i])
        return sprouted_train, sprouted_labels

    @staticmethod
    def cut_off_attribs(image, start_loc, shape_attack, shape_health, count, diff=138, shift=5):
        """
        Cut off attributes on minions on the battlefield (lives and attack attribute)
        :param image: Image as an ndarray
        :param start_loc: Location as a tuple (x, y)
        :param shape_attack: Shape for attack indicator image as a tuple (width, height)
        :param shape_health: Shape for health indicator image as a tuple (width, height)
        :param count: Number of minions for which to get attributes
        :param diff: Shift between each of indicators
        :param shift: Attack indicator cut-off shift 
        :return: Dictionary with lists storing attack and health values of every one of minions
        """
        k = 1 if count % 2 == 0 else 0
        idx = {1: 3, 2: 2, 3: 2, 4: 1, 5: 1, 6: 0, 7: 0}

        attributes = {'attack': [], 'lives': []}
        if count == 0:
            return attributes

        for i in range(7-k):
            attributes['lives'].append(
                Factory.cut_off_image(image, (start_loc[0] + (i * diff) + (k * 70), start_loc[1]), shape_health))
            attributes['attack'].append(
                Factory.cut_off_image(image, (start_loc[0]-85 + (i * diff) + (k * 70), start_loc[1]-shift), shape_attack))

        attributes['attack'] = attributes['attack'][idx[count]:7-k - idx[count]]
        attributes['lives'] = attributes['lives'][idx[count]:7-k - idx[count]]

        # # only vizualization
        # fig, axes = plt.subplots(nrows=len(attributes['health']), ncols=2)
        # for i in range(len(attributes['health'])):
        #     if len(attributes['health']) > 1:
        #         axes[i][0].imshow(attributes['attack'][i], cmap=plt.cm.gray)
        #         axes[i][1].imshow(attributes['health'][i], cmap=plt.cm.gray)
        #     else:
        #         axes[0].imshow(attributes['attack'][i], cmap=plt.cm.gray)
        #         axes[1].imshow(attributes['health'][i], cmap=plt.cm.gray)
        # plt.show()

        return attributes

    @staticmethod
    def minions_count(image):
        """
        Get number of minions in a given image
        :param image: Image as an ndarray
        :return: Number of minions
        """
        minions_gray = skimage.color.rgb2gray(image)
        minions_sobel = skimage.filters.sobel(minions_gray)

        segment = minions_sobel.copy()
        segment[minions_sobel < 10.0 / 255.0] = 0
        segment[minions_sobel >= 50.0 / 255.0] = 1
        segment = ndimage.binary_fill_holes(segment)

        segment = skimage.morphology.opening(segment, skimage.morphology.square(10))
        segment = skimage.morphology.erosion(segment, skimage.morphology.square(30))

        b = skimage.measure.label(segment)
        return len(np.unique(b)) - 1

    @staticmethod
    def opponent_cards(image, binary_masks_folder='data/cards_masks/'):
        """
        Detect the number of opponent's cards in hand
        :param image: Image as an ndarray
        :param binary_mask: Binary mask to make detection more precise
        :return: Number of opponent's cards in hand
        """
        image_gray = skimage.color.rgb2gray(image)

        segmented = np.zeros_like(image_gray)
        segmented[image_gray > 80.0 / 255.0] = 1

        segmented = skimage.morphology.closing(segmented, skimage.morphology.disk(2))
        segmented = ndimage.binary_fill_holes(segmented)

        results = {}
        for f in listdir(binary_masks_folder):
            name = f[:-4]
            f = path.join(binary_masks_folder, f)
            img = skimage.io.imread(f)
            img = skimage.color.rgb2gray(img)
            match_result = match_template(segmented, img)
            results[name] = np.squeeze(match_result)

        key, value = max(results.iteritems(), key=lambda x: x[1])
        # print("Best match has '%s' with prob=%s" % (key, value))
        return int(key)

    @staticmethod
    def player_cards(image, binary_mask='data/player_mask.png'):
        """
        Detect the number of player's cards in hand
        :param image: Image as an ndarray
        :param binary_mask: Binary mask to make detection more precise
        :return: Number of player's cards in hand 
        """
        mask = skimage.io.imread(binary_mask)
        mask = skimage.color.rgb2gray(mask)

        image_gray = skimage.color.rgb2gray(image)
        segmented = np.zeros_like(image_gray)
        segmented[image_gray == 1.0] = 1
        segmented[mask > 0.0] = 0

        segmented = skimage.morphology.closing(segmented, skimage.morphology.disk(5))
        segmented = skimage.morphology.dilation(segmented, skimage.morphology.disk(3))

        # Remove objects smaller than 75 values together
        segmented = np.asarray(segmented * 255.0, dtype=np.bool)
        skimage.morphology.remove_small_objects(segmented, min_size=75, in_place=True)
        segmented = np.asarray(segmented, dtype=np.uint8)

        labeled = skimage.measure.label(segmented, background=0)
        return len(np.unique(labeled)) - 1

    @staticmethod
    def mana_active(image):
        """
        Detect the number of active mana crystals
        :param image: Image as an ndarray
        :return: Number of active mana crystals
        """
        pom = image[:, :, 2]

        segmented = np.zeros_like(pom)
        segmented[pom > 225] = 1
        a = segmented

        pom = image[:, :, 0]
        segmented = np.zeros_like(pom)
        segmented[pom > 205] = 1
        segmented = skimage.morphology.dilation(segmented, skimage.morphology.disk(8))
        b = segmented

        segmented = (a + b) / 2
        segmented = skimage.morphology.erosion(segmented, skimage.morphology.disk(2))
        segmented = skimage.morphology.dilation(segmented, skimage.morphology.disk(3))

        b = skimage.measure.label(segmented)
        return len(np.unique(b)) - 1

    @staticmethod
    def mana_passive(image):
        """
        Detect the number of passive  mana crystals
        :param image: Image as an ndarray
        :return: Number of passive mana crystals
        """
        mana = image[:, :, 0]
        segmented = np.zeros_like(mana)
        segmented[mana < 25] = 1

        segmented = sobel(segmented)
        segmented = ndimage.binary_fill_holes(segmented)

        segmented = skimage.morphology.erosion(segmented, skimage.morphology.star(5))
        segmented = skimage.morphology.dilation(segmented, skimage.morphology.star(4))
        segmented = skimage.morphology.dilation(segmented, skimage.morphology.diamond(3))
        segmented = skimage.morphology.erosion(segmented, skimage.morphology.disk(3))

        b = skimage.measure.label(segmented)
        return len(np.unique(b)) - 1

    @staticmethod
    def turn(image):
        """
        Get the actual turn number
        :param image: Image as an ndarray
        :return: Actual turn number
        """
        p = Factory.mana_passive(image)
        a = Factory.mana_active(image)
        return p + a

    @staticmethod
    def players_turn(image):
        """
        Detect who's on turn, whether the player or the opponent
        :param image: Image as an ndarray
        :return: True if the player's on turn, otherwise False (opponent's turn)
        """
        # turn_img = Factory.cut_off_image(image, (1450, 430), (210, 125))
        b_image = np.invert(image[:, :, 2])

        neg = np.zeros_like(b_image)
        neg[b_image > 220] = 1

        neg = skimage.morphology.closing(neg, skimage.morphology.square(7))
        for i in range(4):
            neg = skimage.morphology.erosion(neg, skimage.morphology.square(7))

        c = len(neg[neg == 1])
        return True if c > 100 else 0

    @staticmethod
    def hero_class(image, templates_folder='data/classes/'):
        """
        Detect and classify what kind of hero is in image using template matching method
        :param image: Image as an ndarray
        :param templates_folder: Folder with template images for Template Matching method
        :return: Name and class of a hero as a tuple
        """
        results = {}
        for f in listdir(templates_folder):
            name = f[:-4]
            f = path.join(templates_folder, f)
            img = skimage.io.imread(f)
            vyrez = Factory.cut_off_image(img, (40, 50), (190, 190))
            vyrez_resized = skimage.transform.resize(vyrez, (140, 140))
            vyrez_resized = vyrez_resized[:, :, 0:3]
            match_result = match_template(image, vyrez_resized)
            results[name] = np.squeeze(match_result)

        # print(results)
        key, value = max(results.iteritems(), key=lambda x: x[1])
        # print("Best match has '%s' with prob=%s" % (key, value))

        name = key.split('_')[1].replace('-', ' ')
        hclass = key.split('_')[0]  # TODO: Rename images with first letter of class name in uppercase
        hclass = hclass[0].upper() + hclass[1:]

        return name, hclass


class KNNClassifier:
    """ Class representing K-Nearest Neighbour classifier """
    n_neighbours = None

    def __init__(self, n_neighbours=3):
        self._model = None
        self.n_neighbours = n_neighbours

    def save(self, filename):
        """ 
        Save model via cPickle as a binary file
        :param filename: Name of a file where model will be stored
        """
        with open(filename, 'wb') as output:
            # TODO: Create folder if does not exist
            cPickle.dump(self._model, output)

    def load(self, filename):
        """
        Load model via cPickle from a binary file
        :param filename: Name of a file where model is stored
        """
        with open(filename, 'rb') as model:
            self._model = cPickle.load(model)

    @staticmethod
    def get_features(img, pixels_per_cell=(4, 4), cells_per_block=(5, 5)):
        """
        Compute and get HoG feature vector of an input image
        :param img: Input ndarray gray-scale image
        :param pixels_per_cell: Pixels per cell for HoG descriptor
        :param cells_per_block: Cells per block for HoG descriptor
        :return: Feature vector from HoG descriptor
        """
        features = hog(img, orientations=8, pixels_per_cell=pixels_per_cell, cells_per_block=cells_per_block)

        return features

    def _train_knn(self, train_data, train_labels):
        """
        (class method) Train and fit KNN classifier using labeled train data set
        :param train_data: Train data set
        :param train_labels: Labels for train data set
        """
        self._model = KNeighborsClassifier(n_neighbors=self.n_neighbours)
        self._model.fit(train_data, train_labels)

    def train(self, train_data, train_labels):
        """
        Train KNN classifier using labeled train data set
        :param train_data: Train data set
        :param train_labels: Labels for train data set
        """
        train_features = []
        for i in range(len(train_data)):
            img = train_data[i]
            fv = KNNClassifier.get_features(img)
            train_features.append(fv)

        train_features = np.asarray(train_features, dtype=np.float32)
        train_labels = np.asarray([train_labels])  # REMOVED: dtype=np.int32
        train_labels = np.squeeze(train_labels)
        self._train_knn(train_features, train_labels)

    def classify(self, data):
        """
        Classify unlabeled data into some class as a result of pretrained model prediction 
        :param data: Input unlabeled data to classify into
        :return: List of predicted labels
        """
        data_features = []
        for i in range(len(data)):
            img = data[i]
            fv = KNNClassifier.get_features(img)
            data_features.append(fv)

        data_features = np.asarray(data_features, dtype=np.float32)
        predicted = self._model.predict(data_features)
        return np.squeeze(predicted)
