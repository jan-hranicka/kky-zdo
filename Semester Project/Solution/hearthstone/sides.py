#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This module provides Player and Opponent classes that fully describe Hearthstone in-game battlefield
"""
from hearthstone.processing import Factory, PartialImage
from skimage.transform import resize
import skimage.color
import numpy as np

# CONSTANTS
# [Player]
PLAYER_MINIONS = PartialImage((450, 500), (1000, 200))
PLAYER_HERO_LIVES = PartialImage((1010, 865), (43, 63))
PLAYER_CARDS = PartialImage((590, 930), (630, 150))
PLAYER_HERO_CLASS = PartialImage((895, 760), (140, 140))
PLAYER_MINIONS_ATTRIBS = PartialImage(loc=(567, 614), shape={'attack': (43, 46), 'lives': (31, 43)})
MANA = PartialImage((1315, 980), (340, 40))
TURN = PartialImage((1450, 430), (210, 125))

# [Opponent]
OPPONENT_MINIONS = PartialImage((450, 300), (1000, 200))
OPPONENT_HERO_LIVES = PartialImage((1012, 233), (43, 63))
OPPONENT_CARDS = PartialImage((750, 0), (410, 65))
OPPONENT_HERO_CLASS = PartialImage((895, 130), (140, 140))
OPPONENT_MINIONS_ATTRIBS = PartialImage(loc=(570, 427), shape={'attack': (43, 46), 'lives': (31, 43)})


class Side:
    """
    Class that stores basic properties to describe Player/Opponent side objects
    """
    def __init__(self):
        """ Initializer of Side object """
        self._hero_lives = 0
        self._hero_class = None
        self._hero_name = None
        self._minions_on_board = 0
        self._cards = 0
        self._minions_lives = []
        self._minions_attack = []
        self._on_turn = None
        self._mana_active = 0
        self._mana_passive = 0
        self._turn = 1

    @property
    def hero_lives(self):
        """
        Get number of hero lives
        :return: Number of hero lives
        """
        return self._hero_lives

    @hero_lives.setter
    def hero_lives(self, value):
        """
        Set number of hero lives
        :param value: Number of hero lives
        """
        self._hero_lives = value

    @property
    def hero_class(self):
        """
        Get the name of hero class
        :return: Name of hero class
        """
        return self._hero_class

    @hero_class.setter
    def hero_class(self, value):
        """
        Set the name of hero class
        :param value: Name of hero class
        """
        self._hero_class = value

    @property
    def hero_name(self):
        """
        Set hero's name
        :return: Name of hero
        """
        return self._hero_name

    @hero_name.setter
    def hero_name(self, value):
        """
        Set hero's name
        :param value: Name of hero
        """
        self._hero_name = value

    @property
    def minions_on_board(self):
        """
        Get the number of minions on the board
        :return: Number of minions on the board
        """
        return self._minions_on_board

    @minions_on_board.setter
    def minions_on_board(self, value):
        """
        Set the number of minions on the board
        :param value: Number of minions on the board
        """
        self._minions_on_board = value

    @property
    def cards(self):
        """
        Get the number of cards in hand
        :return: Number of cards in hand
        """
        return self._cards

    @cards.setter
    def cards(self, value):
        """
        Set the number of cards in hand
        :param value: Number of cards in hand
        """
        self._cards = value

    @property
    def minions_lives(self):
        """
        Get the number of lives of each of minions as a list of values
        :return: Number of lives of each of minions as a list of values
        """
        return self._minions_lives

    @minions_lives.setter
    def minions_lives(self, value):
        """
        Set the number of lives of each of minions as a list of values
        :param value: Number of lives of each of minions as a list of values
        """
        self._minions_lives = value

    @property
    def minions_attack(self):
        """
        Get amount of attack of each of minions as a list of values
        :return: Amount of attack of each of minions as a list of values
        """
        return self._minions_attack

    @minions_attack.setter
    def minions_attack(self, value):
        """
        Set amount of attack of each of minions as a list of values
        :param value: Amount of attack of each of minions as a list of values
        """
        self._minions_attack = value

    @property
    def on_turn(self):
        """
        Get whether player is on turn or not
        :return: True if player is on turn, otherwise False
        """
        return self._on_turn

    @on_turn.setter
    def on_turn(self, value):
        """
        Set whether player is on turn or not
        :param value: True if player is on turn, otherwise False
        """
        self._on_turn = value

    @property
    def turn(self):
        """
        Get the number of an actual turn
        :return: number of an actual turn
        """
        return self._turn

    @turn.setter
    def turn(self, value):
        """
        Set the number of an actual turn
        :param value: Number of an actual turn
        """
        self._turn = value

    @property
    def mana_active(self):
        """
        Get amount of active mana crystals
        :return: Amount of active mana crystals
        """
        return self._mana_active

    @mana_active.setter
    def mana_active(self, value):
        """
        Set amount of active mana crystals
        :param value: Amount of active mana crystals
        """
        self._mana_active = value

    @property
    def mana_passive(self):
        """
        Get amount of passive mana crystals
        :return: Amount of passive mana crystals
        """
        return self._mana_passive

    @mana_passive.setter
    def mana_passive(self, value):
        """
        Set amount of passive mana crystals
        :param value: Amount of passive mana crystals
        """
        self._mana_passive = value

    def __str__(self):
        """ Formatted string representation of a Side object """
        return "Minions: %s\nHero lives: %s\nIs on turn: %s\nTurn: %s\nActive mana: %s\nPassive mana: %s" % \
               (self.minions_on_board, self.hero_lives, self.on_turn, self.turn, self.mana_active, self.mana_passive)


class Player(Side):
    """ Class describing objects for a Player side in Hearthstone game """
    def __init__(self, image, health_model, attack_model):
        """
        Initialize Player side object
        :param image: Full screenshot from Hearthstone game
        :param health_model: Classifier for predicting lives of hero and/or minions
        :param attack_model: Classifier for predicting attack of minions
        """
        Side.__init__(self)

        self._minions_image = Factory.cut_off_image(image, PLAYER_MINIONS.loc, PLAYER_MINIONS.shape)
        self._hero_lives = Factory.cut_off_image(image, PLAYER_HERO_LIVES.loc, PLAYER_HERO_LIVES.shape)
        self._cards = Factory.cut_off_image(image, PLAYER_CARDS.loc, PLAYER_CARDS.shape)
        self._hero_class = Factory.cut_off_image(image, PLAYER_HERO_CLASS.loc, PLAYER_HERO_CLASS.shape)
        self._on_turn = Factory.cut_off_image(image, TURN.loc, TURN.shape)

        self.hero_lives = health_model.classify([skimage.color.rgb2gray(self._hero_lives)])
        self.minions_on_board = Factory.minions_count(skimage.color.rgb2gray(self._minions_image))
        self.cards = Factory.player_cards(self._cards)
        self.on_turn = Factory.players_turn(self._on_turn)
        mana_bgr = Factory.cut_off_image(image, MANA.loc, MANA.shape)
        self.turn = Factory.turn(mana_bgr)
        self.mana_active = Factory.mana_active(mana_bgr)
        self.mana_passive = Factory.mana_passive(mana_bgr)

        if self.minions_on_board > 0:
            attribs = Factory.cut_off_attribs(skimage.color.rgb2gray(image), PLAYER_MINIONS_ATTRIBS.loc,
                                              PLAYER_MINIONS_ATTRIBS.shape['attack'], PLAYER_MINIONS_ATTRIBS.shape[
                                                  'lives'], self.minions_on_board, diff=139, shift=3)
            for i in range(len(attribs['lives'])):
                attribs['lives'][i] = resize(attribs['lives'][i], (63, 43), preserve_range=True)
            self.minions_attack = np.squeeze(attack_model.classify(attribs['attack']))
            self.minions_lives = np.squeeze(health_model.classify(attribs['lives']))
        self.hero_name, self.hero_class = Factory.hero_class(self._hero_class)

    def get_descriptor(self):
        """
        Get descriptor of Player side object as a structured dictionary
        :return: Structured dictionary with described object name as a key and predicted value as a value
        """
        desc = dict()
        desc['player hero lives'] = self.hero_lives
        desc['player hero name'] = self.hero_name
        desc['player hero class'] = self.hero_class
        desc['player cards'] = self.cards
        desc['mana active'] = self.mana_active
        desc['mana passive'] = self.mana_passive
        desc['turn number'] = self.turn
        desc['turn'] = 'player' if self.on_turn else 'opponent'
        desc['player minions count'] = self.minions_on_board
        desc['player minions attribs'] = {'attack': self.minions_attack, 'lives': self.minions_lives}
        return desc

    def __str__(self):
        # TODO: Add toString() implementation
        pass


class Opponent(Side):
    """ Class describing objects for a Opponent side in Hearthstone game """
    def __init__(self, image, health_model, attack_model):
        """
        Initialize Opponent side object
        :param image: Full screenshot from Hearthstone game
        :param health_model: Classifier for predicting lives of hero and/or minions
        :param attack_model: Classifier for predicting attack of minions
        """
        Side.__init__(self)

        self._minions_image = Factory.cut_off_image(image, OPPONENT_MINIONS.loc, OPPONENT_MINIONS.shape)
        self._hero_lives = Factory.cut_off_image(image, OPPONENT_HERO_LIVES.loc, OPPONENT_HERO_LIVES.shape)
        self._cards = Factory.cut_off_image(image, OPPONENT_CARDS.loc, OPPONENT_CARDS.shape)
        self._hero_class = Factory.cut_off_image(image, OPPONENT_HERO_CLASS.loc, OPPONENT_HERO_CLASS.shape)
        self._on_turn = Factory.cut_off_image(image, TURN.loc, TURN.shape)

        self.on_turn = not Factory.players_turn(self._on_turn)
        # mana_bgr = Factory.cut_off_image(image, MANA.loc, MANA.shape)
        # self.turn = Factory.turn(mana_bgr)
        # self.mana_active = Factory.mana_active(mana_bgr)
        # self.mana_passive = Factory.mana_passive(mana_bgr)

        self.hero_lives = health_model.classify([skimage.color.rgb2gray(self._hero_lives)])
        self.minions_on_board = Factory.minions_count(skimage.color.rgb2gray(self._minions_image))
        self.cards = Factory.opponent_cards(self._cards)

        if self.minions_on_board > 0:
            attribs = Factory.cut_off_attribs(skimage.color.rgb2gray(image), OPPONENT_MINIONS_ATTRIBS.loc,
                                              OPPONENT_MINIONS_ATTRIBS.shape['attack'],
                                              OPPONENT_MINIONS_ATTRIBS.shape['lives'], self.minions_on_board)
            for i in range(len(attribs['lives'])):
                attribs['lives'][i] = resize(attribs['lives'][i], (63, 43), preserve_range=True)
            self.minions_attack = np.squeeze(attack_model.classify(attribs['attack']))
            self.minions_lives = np.squeeze(health_model.classify(attribs['lives']))
        self.hero_name, self.hero_class = Factory.hero_class(self._hero_class)

    def get_descriptor(self):
        """
        Get descriptor of Opponent side object as a structured dictionary
        :return: Structured dictionary with described object name as a key and predicted value as a value
        """
        desc = dict()
        desc['opponent hero lives'] = self.hero_lives
        desc['opponent hero name'] = self.hero_name
        desc['opponent hero class'] = self.hero_class
        desc['opponent cards'] = self.cards
        desc['opponent minions count'] = self.minions_on_board
        desc['opponent minions attribs'] = {'attack': self.minions_attack, 'lives': self.minions_lives}
        return desc

    def __str__(self):
        # TODO: Add toString() implementation
        pass
