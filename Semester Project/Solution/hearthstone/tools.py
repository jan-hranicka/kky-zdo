import skimage
import skimage.io
import skimage.transform
import numpy as np
import matplotlib.pyplot as plt
from os import listdir
from os.path import join
from matplotlib.patches import Rectangle
import hearthstone.sides as hsd

from hearthstone.processing import Factory


def get_images_list(folder, resolution=(1080, 1920)):
    images_list = []
    for f in listdir(folder):
        img = skimage.io.imread(join(folder, f))
        img = skimage.transform.resize(img, resolution)
        # img = np.asarray(img, np.uint8)

        # plt.show(img)
        # plt.show()
        images_list.append(img)
    return images_list


def put_together(flist, loc, shape):
    result = None
    for image in flist:
        img = Factory.cut_off_image(image, loc, shape)
        result = img if result is None else np.vstack((result, img))
    return result


def map_values(csv_row, folder='data/images'):
    desc = dict()
    for key, val in csv_row.iteritems():
        if val != val:
            desc[key] = None
            if 'attribs' in key:
                desc[key] = {'attack': [], 'lives': []}
            continue

        if 'attribs' in key:
            split_attribs = val.split(';')
            attribs = dict()

            attack = []
            lives = []
            for i in range(len(split_attribs)):
                pom = np.asarray(split_attribs[i].split('/'), np.uint8)
                attack.append(pom[0])
                lives.append(pom[1])

            attribs['attack'] = np.squeeze(np.asarray(attack, np.uint8))
            attribs['lives'] = np.squeeze(np.asarray(lives, np.uint8))
            desc[key] = attribs
        elif 'filename' in key:
            desc[key] = join(folder, val)
        else:
            desc[key] = val
    return desc


def compare_descriptors(descA, descB):
    comp = dict()
    for key, val in descA.iteritems():
        if key not in descB.keys():
            print("Key '%s' is not in descriptor B" % key)
            continue

        if 'attribs' in key:
            for k, v in val.iteritems():
                if np.array_equal(v, descB[key][k]):
                    print("Parametr '%s:%s' se shoduje! %s" % (key, k, v))
                    comp['%s:%s' % (key, k)] = True
                else:
                    print("Parametr '%s:%s' se neshoduje! %s != %s" % (key, k, v, descB[key][k]))
                    comp['%s:%s' % (key, k)] = False
            continue

        if val == descB[key]:
            print("Parametr '%s' se shoduje! (%s)" % (key, val))
            comp[key] = True
        else:
            print("Parametr '%s' se neshoduje! (%s) != (%s)" % (key, val, descB[key]))
            comp[key] = False
    return comp


def make_attrs_string(d):
    attr = []
    if isinstance(d.values()[0], list):
        return ''

    if d.values()[0].shape == ():
        return '%s/%s' % (d.values()[0], d.values()[1])
    for i in range(len(d.values()[0])):
        attr.append('%s/%s' % (d.values()[0][i], d.values()[1][i]))
    return ';'.join(attr)


def print_descriptor(desc):
    print('\n{:<25}  {:<20}'.format('Object', 'Value'))
    print(''.join(['-' for i in range(45)]))
    for key, value in sorted(desc.items()):
        if 'attribs' not in key:
            print('{:<25}  {:<20}'.format(key, value))
        else:
            print('{:<25}  {:<20}'.format(key, make_attrs_string(value)))
    print(''.join(['-' for i in range(45)]))


def visualize_descriptor(desc):
    # TODO: Implement this method
    image = skimage.io.imread(desc['filename'])

    # Resize image if it's not FullHD
    image = skimage.transform.resize(image, (1080, 1920), preserve_range=True)
    image = np.asarray(image, dtype=np.uint8)

    fig, ax = plt.subplots(nrows=1, ncols=1)
    ax.imshow(image)

    p_desc = [desc['player minions count'], desc['player cards'], desc['player hero name'], desc['player hero lives'],
         desc['turn']]
    p_val = [hsd.PLAYER_MINIONS, hsd.PLAYER_CARDS, hsd.PLAYER_HERO_CLASS, hsd.PLAYER_HERO_LIVES, hsd.TURN]
    for d, x in zip(p_desc, p_val):
        rect = Rectangle(xy=list(x.loc), width=x.shape[0], height=x.shape[1], fill=False, edgecolor='y', linewidth=2)
        ax.text(x.loc[0]+12, x.loc[1]+24, str(d), fontsize=12, color='y', weight='heavy')
        ax.add_patch(rect)

    # hsd.MANA, desc['turn number']
    rect = Rectangle(xy=list(hsd.MANA.loc), width=hsd.MANA.shape[0], height=hsd.MANA.shape[1], fill=False,
                     edgecolor='y', linewidth=2)
    text = "%s : %s + %s" % (desc['turn number'], desc['mana active'], desc['mana passive'])
    ax.text(hsd.MANA.loc[0] + 12, hsd.MANA.loc[1] + 24, text, fontsize=12, color='y', weight='heavy')
    ax.add_patch(rect)

    _visualize_attribs(ax, desc['player minions attribs'], (567, 614), desc['player minions count'])

    p_desc = [desc['opponent minions count'], desc['opponent cards'], desc['opponent hero name'], desc['opponent hero lives']]
    p_val = [hsd.OPPONENT_MINIONS, hsd.OPPONENT_CARDS, hsd.OPPONENT_HERO_CLASS, hsd.OPPONENT_HERO_LIVES]
    for d, x in zip(p_desc, p_val):
        rect = Rectangle(xy=list(x.loc), width=x.shape[0], height=x.shape[1], fill=False, edgecolor='c', linewidth=2)
        ax.text(x.loc[0]+12, x.loc[1]+24, str(d), fontsize=12, color='c', weight='heavy')
        ax.add_patch(rect)

    _visualize_attribs(ax, desc['opponent minions attribs'], (570, 427), desc['opponent minions count'],
                       side='opponent')
    plt.show()


def _visualize_attribs(ax, desc, loc, count, side='player'):
    k = 1 if count % 2 == 0 else 0
    idx = {1: 3, 2: 2, 3: 2, 4: 1, 5: 1, 6: 0, 7: 0}
    pos_attack = list()
    pos_lives = list()
    if count > 0:
        for i in range(7 - k):
            pos_attack.append((loc[0] - 85 + (i * 138) + (k * 70), loc[1] - 5))
            pos_lives.append((loc[0] + (i * 138) + (k * 70), loc[1] - 5))
        pos_attack = pos_attack[idx[count]:7 - k - idx[count]]
        pos_lives = pos_lives[idx[count]:7 - k - idx[count]]

        color = 'y' if side == 'player' else 'c'
        if len(pos_lives) > 1:
            for i in range(len(pos_lives)):
                rect = Rectangle(xy=list(pos_lives[i]), width=31,
                                 height=43, fill=False, edgecolor=color, linewidth=2)
                ax.text(pos_lives[i][0] + 6, pos_lives[i][1] + 20, str(desc['lives'][i]),
                        fontsize=12, color=color, weight='heavy')
                ax.add_patch(rect)
                rect = Rectangle(xy=list(pos_attack[i]), width=43,
                                 height=46, fill=False, edgecolor=color, linewidth=2)
                ax.text(pos_attack[i][0] + 6, pos_attack[i][1] + 20, str(desc['attack'][i]),
                        fontsize=12, color=color, weight='heavy')
                ax.add_patch(rect)
        else:
            pos_lives = pos_lives[0]
            pos_attack = pos_attack[0]
            rect = Rectangle(xy=list(pos_lives), width=31,
                             height=43, fill=False, edgecolor=color, linewidth=2)
            ax.text(pos_lives[0] + 6, pos_lives[1] + 20, str(desc['lives']),
                    fontsize=12, color=color, weight='heavy')
            ax.add_patch(rect)
            rect = Rectangle(xy=list(pos_attack), width=43,
                             height=46, fill=False, edgecolor=color, linewidth=2)
            ax.text(pos_attack[0] + 6, pos_attack[1] + 20, str(desc['attack']),
                    fontsize=12, color=color, weight='heavy')
            ax.add_patch(rect)
